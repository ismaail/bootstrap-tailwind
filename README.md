### Bootstrap 5 & Tailwind CSS

##### 1 Installation

```sh
npm install
```

<br>

##### 2 Compile assets

```sh
npm run webpack
```

<br>

##### 3 View in the browser at http://localhost:3000 

```
npm run serve
```

port can be changed in `bs-config.json` file 
