const path = require('path');
const webpack = require('webpack');
const tailwindcss = require('tailwindcss');
const autoprefixer = require('autoprefixer');
const { VueLoaderPlugin } = require('vue-loader');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries");
const WebpackErrorNotificationPlugin = require('webpack-error-notification');

let isProd = "production" === process.env.NODE_ENV;

module.exports = {
    entry: {
        app: [
            `${__dirname}/src/js/app.js`,
        ],
        style: [
            `${__dirname}/src/scss/style.scss`,
        ],
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].js',
        pathinfo: false,

    },
    mode: 'development',
    devtool: 'source-map',
    module: {
        rules: [
            {
                loader: 'babel-loader',
                test: /src\/(.+)\.js$/,
                exclude: /node_modules/,
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        js: 'babel-loader',
                    },
                },
            },
            {
                test: /src\/scss\/(.+)\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                plugins: [
                                    tailwindcss('tailwind.config.js'),
                                    autoprefixer(),
                                ],
                            },
                            sourceMap: true,
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sassOptions: {
                                includePaths: ['node_modules/compass-mixins/lib'],
                                sourceMap: true,
                                outputStyle: 'compressed',
                            },
                        },
                    },
                ],
            },
            {
                test: /resources\/assets\/css\/(.+)\.css$/,
                use: [
                    // 'vue-style-loader',
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: !isProd,
                        }
                    },
                ],
            },
        ],
    },
    resolve: {
        modules: [
            `${__dirname}/node_modules`,
        ],
        alias: {
            vue$: 'vue/dist/vue.esm.js',
        },
    },
    plugins: [
        new VueLoaderPlugin(),
        new FixStyleOnlyEntriesPlugin(),
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css",
        }),

        new WebpackErrorNotificationPlugin(process.platform, { notifyWarnings: false }),
    ],

};
