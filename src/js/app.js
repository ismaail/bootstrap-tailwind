import Vue from 'vue';
import AppComponent from './components/AppComponent.vue';
import { store } from './store/store'

Vue.component('app-component', AppComponent)

new Vue({
    store,
    el: '#main-app',
});
