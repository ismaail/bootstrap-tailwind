import Vue from 'vue';
import Vuex from 'vuex';
import faker from "faker";

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        people: [],
    },
    getters: {
        makeList: (state) => {
            return createFakeList();
        },
    },
});

const createFakeList = () =>  {
    const people = [];
    const count = 12;

    for (let i = 0; i < count; i++) {
        people.push({
            avatar: `/images/avatar${(i % 6) + 1}.png`,
            name: faker.name.findName(),
            job: faker.name.jobTitle(),
            email: faker.internet.email(),
            phone: faker.phone.phoneNumber(),
        });
    }

    return people;
};
